[![Pipeline status](https://gitlab.com/ArturGulik/free-software-presentation/badges/main/pipeline.svg)](https://gitlab.com/ArturGulik/free-software-presentation/-/commits/main)

# Free Software Presentation

A web multimedia presentation on Free Software. Created using the [icesmooth framework](https://gitlab.com/ag-projects/icesmooth).

The presentation is prepared for presenting and not reading aloud - this means it does not contain much text and on its own isn't very informative for people who are just browsing. By design, it has to be presented well to be understood well.

# Hosted online

The presentation is available online: [`Free Software Presentation`](https://arturgulik.gitlab.io/free-software-presentation/)